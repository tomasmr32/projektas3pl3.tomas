﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel.Dal.DataAnnotations;

namespace Hotel.Main
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        public void FillForm<T>(T viewModel) where T : class
        {
            //TODO: patikrinti ar class yra viewmodel
            var formProperties = viewModel.GetType().GetProperties();

            foreach (var property in formProperties)
            {
                var customAttributes = property.GetCustomAttributes(false);

                if (customAttributes.Count() != 0)
                {
                    var controlName = ((FormPropertyAttribute)customAttributes.Where(x => x.GetType() == typeof(FormPropertyAttribute)).FirstOrDefault()).ControlName;
                }
            }
        }
    }
}
